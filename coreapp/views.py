from django.shortcuts import render_to_response
from django.template import RequestContext

def home(request):

    d = {}

    return render_to_response('index.html', d)

def close_login_popup(request):
    return render_to_response('close_popup.html', {}, RequestContext(request))
