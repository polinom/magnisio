$(document).ready(function(){

    // methos that counts length of an object
    Object.size = function(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };

    
    document.bascket = {};
    $('.photo').click(function(){
        var el = $(this);
        var photo_id = el.attr('id');
        var photo_url = el.find('img').attr('src');
        ret = el.toggleClass('selected');
        if (el.hasClass('selected')){
          document.bascket[photo_id] = {'photo_url': photo_url, amount: 1};
        } else {
          delete document.bascket[photo_id];
        }

        //get count and update view object
        var count = Object.size(document.bascket);
        $('.amount').html(count);

        var delivery = 0.99;
        //count tottal amount
        if(count===0){ delivery=0; }
        var tottal = count * 1.59 + delivery;
        tottal = Math.round(tottal * 100) / 100;
        $('.tottal').html(tottal + ' $');
    });
});