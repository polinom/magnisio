# project wide urls
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.contrib import admin
from coreapp.views import close_login_popup
admin.autodiscover()

# import your urls from each app here, as needed
import dash.urls

urlpatterns = patterns('',

    # urls specific to this app
    url(r'^dash/', include(dash.urls)),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^admin/', include(admin.site.urls)),

    # catch all, redirect to coreapp home view
    url(r'^$', TemplateView.as_view(template_name='index.html')),

    url(r'', include('social_auth.urls')),

    url(r'^close_login_popup/$', close_login_popup, name='login_popup_close'),

)