from django.views.generic import TemplateView
from instagram.client import InstagramAPI
from instagram.bind import InstagramAPIError
from django.contrib.auth import logout



class Instagram(object):
    def __init__(self, access_token=None, uid=None):
        if not access_token or not uid:
            raise Exception('Please pass credentials for initialisation')
        else:
            self.api = InstagramAPI(access_token=access_token)
            self.uid = uid

    def get_photos(self):
        recent_media, next = self.api.user_recent_media(user_id=self.uid)
        return recent_media

    def get_liked(self):
        recent_media, next = self.api.user_liked_media()
        return recent_media



class DashBoardView(TemplateView):

    def get_context_data(self, **kwargs):
        context = super(DashBoardView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            credentials = self.request.user.social_auth.get(provider='instagram')
            access_token = credentials.extra_data['access_token']
            try:
                api = Instagram(access_token=access_token, uid=credentials.uid)
                context['photos'] = api.get_photos()
                context['liked_photos'] = api.get_liked()
            except InstagramAPIError:
                logout(self.request)
        return context
