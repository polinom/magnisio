from django.conf.urls import patterns, include, url
from django.core.urlresolvers import reverse
from dash.views import DashBoardView


urlpatterns = patterns('',
    url(r'^$', DashBoardView.as_view(template_name='dash.html')),
)
